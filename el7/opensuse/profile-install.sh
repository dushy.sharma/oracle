#!/bin/bash
#
# profile-install.sh

# OpenSUSE Leap version
VERSION=15.2

# Current directory
CWD=$(pwd)

# Slow things down a bit
SLEEP=1

# Make sure the script is being executed with superuser privileges.
if [[ "${UID}" -ne 0 ]]
then
  echo "  Please run with sudo or as root." >&2
  echo
  exit 1
fi

echo 
echo "  === Install client desktop profiles ==="
echo
rm -rf /etc/skel/.config
mkdir /etc/skel/.config
sleep ${SLEEP}

echo "  Installing custom KDE profile for OpenSUSE Leap ${VERSION}."
cp ${CWD}/${VERSION}/kde/* /etc/skel/.config/
sleep ${SLEEP}

echo 

exit 1
