#!/bin/bash
#
# profile-restore.sh

# OpenSUSE Leap version
VERSION=15.2

# Current directory
CWD=$(pwd)

# Defined users
USERS="$(awk -F: '$3 > 999 {print $1}' /etc/passwd | sort)"

# Backup configuration
BACKUPS="libreoffice \
         Microsoft \
         ownCloud \
         soffice.binrc
         teams \
         transmission \
         VirtualBox \
         VirtualBox6rc \
         vlc"

# Slow things down a bit
SLEEP=0.2

# Make sure the script is being executed with superuser privileges.
if [[ "${UID}" -ne 0 ]]
then
  echo "  Please run with sudo or as root." >&2
  echo
  exit 1
fi

echo
echo "  === Apply custom KDE configuration ==="
echo 
sleep ${SLEEP}

if [ ! -d /etc/skel/.config ]
then
  echo "  Custom profiles are not installed." >&2
  echo 
  exit 1
fi

for USER in ${USERS}
do
  if [ -d /home/${USER} ]
  then
    echo "  Updating profile for user: ${USER}"
    rm -rf /home/${USER}/.config.bak
    mv -f /home/${USER}/.config /home/${USER}/.config.bak
    cp -R /etc/skel/.config /home/${USER}/
    for BACKUP in ${BACKUPS}
    do
      if [ -r /home/${USER}/.config.bak/${BACKUP} ]
      then 
        cp -R /home/${USER}/.config.bak/${BACKUP} /home/${USER}/.config/
      fi
    done
    chown -R ${USER}:${USER} /home/${USER}/.config
    sleep ${SLEEP}
  fi
done
echo
